#
# Set-PerlEnvironment.ps1
#
function Set-PerlEnvironment() {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
		[string] $Path
	)

	$configuredPath = $env:Path.Split(';')
	$perlPath =  Join-Path $Path "strawberry-perl" -Resolve -ErrorAction Stop

	if($perlPath -in $configuredPath){
		Write-Verbose "Set-PerlEnvironment -Perl path $Path allready set in environment"	
	} else{
		$env:Path = $env:Path + ";" + (Join-Path $perlPath perl\site\bin) + ";" + (Join-Path $perlPath perl\bin)+ ";" + (Join-Path $perlPath c\bin)
		
	}

	Write-Debug "Set-PerlEnvironment reset perl environment variables"
	$env:TERM = ""
	
	$env:PERL_JSON_BACKEND = ""
	$env:PERL_YAML_BACKEND = ""
	$env:PERL5LIB = ""
	$env:PERL5OPT = ""
	$env:PERL_MM_OPT = ""
	$env:PERL_MB_OPT = ""
	
	& perl -MConfig -e '"printf("""Perl executable: %s\nPerl version   : %vd / $Config{archname}\n""", $^X, $^V)"' 2 | Out-Null
	if($LASTEXITCODE -ne 0) {
		Write-Error "Set-PerlEnvironment - perl does not work; check if your strawberry pack is complete!"
		exit
	}
}


#
# Get_ApacheVersionTag.ps1
#
function Get-ApacheVersionTag {
	param(
		[string] $SourcePath,
		[string] $VSVersion,
		[string] $Platform,
		[switch] $Long
	)

	Push-Location $SourcePath

	try{

		if((Test-Path .\versions.csv) -ne $true)  {
			Write-Error "Get-ApacheVersionTag - no version history found"
			exit
		}
	
		$srcVersions = Import-Csv -Path "versions.csv" -Delimiter ";"
	
		$apacheVersionString += "apache-" + ($srcVersions | where Source -eq "httpd").Version
		$apacheVersionString += "_openssl-" + ($srcVersions | where Source -eq "openssl").Version

		if($Long.IsPresent) {
			$apacheVersionString += "_apr-" + ($srcVersions | where Source -eq "apr").Version
			$apacheVersionString += "_aprutil-" + ($srcVersions | where Source -eq "apr-util").Version
		}

		if($Long.IsPresent) {
			$apacheVersionString += "_pcre-" + ($srcVersions | where Source -eq "pcre").Version
		}

		if($Long.IsPresent) {
			$apacheVersionString += "_zlib-" + ($srcVersions | where Source -eq "zlib").Version
		}

		$apacheVersionString +="_VC$VSVersion" + "_$Platform"

		$apacheVersionString
	}
	finally{
		Pop-Location
	}
}
#
# Invoke_Make.ps1
#

function Invoke-Make {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True)]
		[string] $SourcePath,

		[Parameter(Mandatory=$True)]
		[string] $BuildPath,
		
		[Parameter(Mandatory=$True)]
		[string[]] $InstallPrefix,


		$Arguments = @()
	)

	Write-Verbose "Invoke-Make - build path: $BuildPath"
	Write-Verbose "Invoke-Make - source path: $SourcePath"
	Write-Verbose "Invoke-Make - build arguments: $Arguments"
	if((Test-Path $BuildPath) -ne $true) {
		New-Item $BuildPath -ItemType Directory | Out-Null
	}
	
	$sourcePathResolved = (Resolve-Path $SourcePath -ErrorAction Stop).Path
	$buildName = Split-Path $BuildPath -Leaf
	Push-Location $BuildPath
	

	try{
		$installPrefixOption = "-DCMAKE_INSTALL_PREFIX=" + $InstallPrefix

		Write-Verbose "Invoke-Make - $buildName build - running cmake"
		cmake -Wno-dev -G "NMake Makefiles" $installPrefixOption -DCMAKE_BUILD_TYPE=RelWithDebInfo $Arguments "$sourcePathResolved" 

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-Make - cmake failed"
			exit
		}

		Write-Verbose "Invoke-Make - $buildName build -running nmake"
		nmake /NOLOGO
		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-Make - nmake failed"
			exit
		}

		Write-Verbose "Invoke-Make - $buildName build - running nmake install"
		nmake /NOLOGO install

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-Make - nmake install failed"
			exit
		}

	} finally {
		Pop-Location
	}

}
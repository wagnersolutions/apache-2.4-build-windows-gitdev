#
# Test_ApacheBuild.ps1
#

function Test-ApacheBuildFileVersions {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$true)]
		[string] $InstallPrefix
	)

	$apacheVersionString = & $InstallPrefix\bin\httpd.exe -v
	$apacheVersionString = & $InstallPrefix\bin\httpd.exe -v
	Write-Verbose "Test-ApacheBuild - Apache $apacheVersionString"

	$apacheFileVersion = [system.diagnostics.fileversioninfo]::GetVersionInfo( (Resolve-Path $InstallPrefix\bin\httpd.exe).Path) | Select -ExpandProperty FileVersion
	Write-Verbose "Test-ApacheBuild - Apache file version: $apacheFileVersion"

	Get-ChildItem $InstallPrefix\modules | % { 
		$modulePath = $_.Fullname
		$moduleVersion = [system.diagnostics.fileversioninfo]::GetVersionInfo( $modulePath ) | Select -ExpandProperty FileVersion
		$moduleName = Split-Path $_ -Leaf

		Write-Verbose "Test-ApacheBuild - module $moduleName`tVersion: $moduleVersion "

		if($moduleVersion -ne $apacheFileVersion) {
			Write-Error "Test-ApacheBuild - module $moduleName (version $moduleVersion) has not apache version $apacheFileVersion"
		}

	}

}
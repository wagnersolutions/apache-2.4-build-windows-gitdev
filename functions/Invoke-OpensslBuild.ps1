#
# Invoke_OpensslBuild.ps1
#
function Invoke-OpenSSLBuild {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True)]
		[string] $SourcePath,

		[Parameter(Mandatory=$True)]
		[string] $BuildPath,
		
		[Parameter(Mandatory=$True)]
		[string] $InstallPrefix,

		[Parameter(Mandatory=$True)]
		
		[string] $Platform
	)

	$configurePlatform = "VC-WIN32"

	if($Platform -eq "x64") {
		$configurePlatform = "VC-WIN64A"
	}

	$prefixOption = "--prefix=" + $InstallPrefix
	$opensslDirOption = "--openssldir=" + $InstallPrefix + "\conf"

	if((Test-Path $BuildPath) -ne $true) {
		New-Item $BuildPath -ItemType Directory | Out-Null

		Write-Verbose "Invoke-OpenSSLBuild - copying source to build path"
		Copy-Item $SourcePath $BuildPath -Force -Recurse 
	}
	
	Push-Location (Join-Path $BuildPath (Split-Path $SourcePath -Leaf))
	try{
	
		perl Configure $configurePlatform $prefixOption $opensslDirOption enable-camellia no-idea no-mdc2 no-ssl2 no-ssl3 no-zlib

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-OpenSSLBuild - perl configure failed"
			exit
		}

		if($Platform -eq "x64") {
			.\ms\do_win64a.bat }
		else {
			.\ms\do_nasm.bat 
		}

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-OpenSSLBuild - openssl compile batch failed"
			exit
		}

		nmake /NOLOGO /f ms\ntdll.mak 

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-OpenSSLBuild - nmake failed"
			exit
		}

		nmake /NOLOGO /f ms\ntdll.mak install

		if($LASTEXITCODE -ne 0) {
			Write-Error "Invoke-OpenSSLBuild - nmake install failed"
			exit
		}
	} 
	finally {
		Pop-Location 
	}
}
﻿
function Get-ApacheSources{
   [CmdletBinding()]
	param
	(
		[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
		[string] $Path
	)

	$ErrorActionPreference = "Stop"

	Write-Verbose "Get-ApacheSources - checking for missing tools"

	if((Test-Path $Path) -ne $true) {
		New-Item $Path -ItemType Directory | Out-Null
	}

	Push-Location $Path

	try{

		$srcVersions = @()
		$updateableVersions = @()

		# APR
		Write-Verbose "Get-ApacheSources - APR - searching current version on apache website"
		$content = (Invoke-WebRequest "http://apr.apache.org/download.cgi" -ErrorAction Stop) 
		$downloadSourceLink = ($content.ParsedHtml.links | where innerText -Like "apr-*win32*.zip" | Select -First 1)

		$srcVersion= New-Object -TypeName PSobject -Property @{
							Source = "apr"
							Version = $downloadSourceLink.innerText.Replace("apr-", "").Replace("-win32-src.zip", "")
							HRef = $downloadSourceLink.href }
		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - APR - found version: " + $srcVersion.Version)

		# APR-UTIL
		Write-Verbose "Get-ApacheSources - APR-UTIL - searching current version on apache website"
		$content = (Invoke-WebRequest "http://apr.apache.org/download.cgi" -ErrorAction Stop) 
		$downloadSourceLink = ($content.ParsedHtml.links | where innerText -Like "apr-util*win32*.zip" | Select -First 1)
    
		$srcVersion=  New-Object -TypeName PSobject -Property @{
							Source = "apr-util"
							Version = $downloadSourceLink.innerText.Replace("apr-util-", "").Replace("-win32-src.zip", "")
						HRef = $downloadSourceLink.href }
		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - APR-UTIL - found version: " + $srcVersion.Version)

		# HTTPD
		Write-Verbose "Get-ApacheSources - HTTPD - searching current version on apache website"
		$content = (Invoke-WebRequest "http://httpd.apache.org/download.cgi")
		$downloadSourceLink = ($content.ParsedHtml.links | where innerText -Like "httpd-2.4.*tar.gz" | Select -First 1)
		$srcVersion= New-Object -TypeName PSobject -Property @{
							Source = "httpd"
							Version = $downloadSourceLink.innerText.Replace("httpd-", "").Replace(".tar.gz", "")
						HRef = $downloadSourceLink.href }
		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - HTTPD - found version: "+ $srcVersion.Version)

		# PCRE
		Write-Verbose "Get-ApacheSources - PCRE - searching current version on sourceforge"
		$content = (Invoke-WebRequest "http://sourceforge.net/projects/pcre/files/pcre/")
		$pcreVersion = (($content.ParsedHtml.links | Where innerText -like "8.*" | Select -First 1).innerText).Trim()
		$downloadSourceLink = "http://sourceforge.net/projects/pcre/files/pcre/$pcreVersion/pcre-$pcreVersion.zip/download"
		$srcVersion= New-Object -TypeName PSobject -Property @{
							Source = "pcre"
							Version = $pcreVersion
						HRef = $downloadSourceLink }
		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - PCRE - found version: " + $srcVersion.Version)

		#ZLIB
		Write-Verbose "Get-ApacheSources - ZLIB - searching current version on zlib.net"
		$downloadSourceLink = "http://zlib.net/zlib1211.zip"
		$srcVersion= New-Object -TypeName PSobject -Property @{
							Source = "zlib"
							Version = ( $downloadSourceLink -as [System.URI]).AbsolutePath.Replace(".zip", "").Replace("/zlib", "")
						HRef = $downloadSourceLink }

		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - ZLIB - found version: " + $srcVersion.Version)

		#OPENSSL
		Write-Verbose "Get-ApacheSources - OPENSSL - searching current version on openssl.org"
		$content = (Invoke-WebRequest "http://www.openssl.org/source/")
		$downloadSourceLink = "http://www.openssl.org/source/" + ($content.ParsedHtml.links| Select innerText |  Where innerText -Like "openssl-1.0.2*.tar.gz" | Select -Last 1).innerText
		$srcVersion=  New-Object -TypeName PSobject -Property @{
							Source = "openssl"
							Version =  $downloadSourceLink.Replace("http://www.openssl.org/source/openssl-", "").Replace(".tar.gz", "")
						HRef = $downloadSourceLink }
		$srcVersions+= $srcVersion	
		Write-Verbose ("Get-ApacheSources - OPENSSL - found version: " + $srcVersion.Version)

		Write-Verbose "Get-ApacheSources - comparing found versions with download history"
		if((Test-Path .\versions.csv) -eq $true)  {
			$lastVersions = Import-Csv -Path "versions.csv" -Delimiter ";"

			foreach($lastVersion in $lastVersions){
				$currentVersion = $srcVersions | where Source -eq $lastVersion.Source
				if($lastVersion.Version -lt $currentVersion.Version) {
					$updateableVersions+= $currentVersion
					Write-Verbose ("Get-ApacheSources - update found for " + $srcVersion.Source)
				
					$path = $currentVersion.Source

					if((Test-Path $path) -eq $true)  {
						Write-Verbose ("Get-ApacheSources - removing old sources of " + $srcVersion.Source)
						Remove-Item $path -Recurse
					}
            
				}
			}
		}


		if((Test-Path .\apr) -ne $true)  {
			Write-Verbose "Get-ApacheSources - downloading sources for apr..."
			Get-ZippedSource -Source ($srcVersions | where Source -eq "apr").Href -Target .\apr
		}


		if((Test-Path .\apr-util) -ne $true)  {
			Write-Verbose "Get-ApacheSources - downloading sources for apr-util..."
			Get-ZippedSource -Source ($srcVersions | where Source -eq "apr-util").Href -Target .\apr-util
		}


		if((Test-Path .\httpd) -ne $true)  {
    		Write-Verbose "Get-ApacheSources - downloading sources for httpd..."

			Get-TarballSource -Source ($srcVersions | where Source -eq "httpd").Href -Target .\httpd

		}

		if((Test-Path .\pcre) -ne $true)  {
			Write-Verbose "Get-ApacheSources - downloading sources for pcre..."
			Get-ZippedSource -Source ($srcVersions | where Source -eq "pcre").Href -Target .\pcre
		}

		if((Test-Path .\zlib) -ne $true)  {
			Write-Verbose "Get-ApacheSources - downloading sources for zlib..."
			Get-ZippedSource -Source ($srcVersions | where Source -eq "zlib").Href -Target .\zlib
		}

		if((Test-Path .\openssl) -ne $true)  {    
			Write-Verbose "Get-ApacheSources - downloading sources for openssl..."
			Get-TarballSource -Source ($srcVersions | where Source -eq "openssl").Href -Target .\openssl
		}

		Write-Verbose "Get-ApacheSources -updating version history..."
		$srcVersions | Export-Csv -Path .\versions.csv -Delimiter ";" -NoTypeInformation

	}
	finally {
		Pop-Location
	}

}
#
# Get_RedirectedUrl.ps1
#

Function Get-RedirectedUrl {
 
    Param (
        [Parameter(Mandatory=$true)]
        [String]$URL
    )
 
     $request = [System.Net.WebRequest]::Create($url)
     $request.AllowAutoRedirect=$true

    try{
         $response=$request.GetResponse()
         $response.ResponseUri.AbsoluteUri
         $response.Close()
     }

    catch{
        Write-Error �Get-RedirectedUrl: $_�
    }

}
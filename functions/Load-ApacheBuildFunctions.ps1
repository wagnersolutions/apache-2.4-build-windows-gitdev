#
# Load-ApacheBuildFunctions.ps1
#

#helpers
. .\Apply-Patch.ps1
. .\Get-RedirectedUrl.ps1
. .\Get-TarballSource.ps1
. .\Get-ZippedSource.ps1
. .\Invoke-Environment.ps1

# build environment
. .\Set-PerlEnvironment.ps1
. .\Set-VisualStudioEnvironment.ps1

#downloaders
. .\Get-ApacheBuildTools.ps1
. .\Get-ApacheSources.ps1

# building
. .\Invoke-Make.ps1
. .\Invoke-OpensslBuild.ps1
. .\Get-ApacheVersionTag.ps1

#tests
. .\Test-ApacheBuildFileVersions.ps1




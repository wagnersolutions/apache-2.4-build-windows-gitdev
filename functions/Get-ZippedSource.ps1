#
# Get_ZippedSource.ps1
#
function Get-ZippedSource {
  [CmdletBinding()]
  param
  (
    [string]$Source,
    [string]$Target
  )

  process {
    
    if($Source -eq $null){
        Write-Error "source missing"
        return
    }

    if($Target -eq $null){
        Write-Error "target missing"
        return
    }

    

    $tempFolder = New-Item -Path (Join-Path $env:TEMP ([System.Guid]::NewGuid().toString())) -ItemType Directory
    
	Write-Debug "Get-ZippedSource - temp folder:  $tempFolder"
    try{
				
        $Source = Get-RedirectedUrl $Source
        Write-Verbose "Get-ZippedSource - get $Source"

		Push-Location $tempFolder
		try{
            Invoke-WebRequest  $Source -UseBasicParsing -OutFile .\source.zip
			& 7za.exe x -oextract "source.zip" | Write-Debug
			$extractedFolders = (Get-Item .\extract).GetDirectories() | Select -ExpandProperty FullName
		} finally {
			Pop-Location
		}

        $extractedFolders | % { Move-Item $_ $Target }
    }
    finally{
        Remove-Item $tempFolder -Recurse -ErrorAction SilentlyContinue
    }
  }

}
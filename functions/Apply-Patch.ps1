#
# Apply_Patch.ps1
#
function Apply-Patch() {
  [CmdletBinding()]
  param
  (
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    [string] $Path,
    [int] $StripPath=1
  )

  process{
    $patchFile = Join-Path $global:PatchPath $Path
    if((Test-Path $patchFile) -ne $true) {
        Write-Error "PatchFile not found at $patchFile"
        return
    }

    & patch.exe ("-p"+ $StripPath) -i $patchFile | Write-Verbose
  }
}
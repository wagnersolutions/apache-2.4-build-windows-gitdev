﻿

function Get-ApacheBuildTools{
   [CmdletBinding()]
	param
	(
		[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
		[string] $Path
	)

	$perlSource = "http://strawberryperl.com/download/5.20.2.1/strawberry-perl-5.20.2.1-64bit-portable.zip"
	$7zipSource = "http://7-zip.org/a/7za920.zip"
	$cMakeSource = "http://www.cmake.org/files/v3.2/cmake-3.2.2-win32-x86.zip"
	$awkSource = "http://gnuwin32.sourceforge.net/downlinks/gawk-bin-zip.php"
	$nasmSource = "http://www.nasm.us/pub/nasm/releasebuilds/2.11.08/win32/nasm-2.11.08-win32.zip"
	$expatSource = "https://github.com/libexpat/libexpat/releases/download/R_2_2_5/expat-win32bin-2.2.5.exe"

	$ErrorActionPreference = "Stop"

	Write-Verbose "Get-ApacheBuildTools - checking for missing tools"

	if((Test-Path $Path) -ne $true) {
		New-Item $Path -ItemType Directory | Out-Null
	}
	
	$resolvedPath = (Resolve-Path $Path).Path
	Write-Debug "Get-ApacheBuildTools - changing location to '$Path'"
	Push-Location $Path

	try{
     	
	    if((Test-Path .\7za.exe) -ne $true) {
			 Write-Verbose "Get-ApacheBuildTools - downloading 7z..."
		     Invoke-WebRequest $7zipSource -UseBasicParsing -OutFile 7z.zip 

		     $shell = new-object -ComObject Shell.Application
		     $zip = $shell.NameSpace((Join-Path $resolvedPath 7z.zip))

		     foreach($item in $zip.Items() | where Path -like "*7za.exe" )
		     {
			    $shell.Namespace($resolvedPath).copyhere($item)
		     }

		     Remove-Item 7z.zip
	    }

	    if((Test-Path .\strawberry-perl) -ne $true) {
		    Write-Verbose "Get-ApacheBuildTools - downloading strawberry-perl..."
		    Invoke-WebRequest $perlSource -UseBasicParsing -OutFile  .\strawberry-perl.zip
			Write-Verbose "Get-ApacheBuildTools - extracting strawberry-perl..."
		    .\7za.exe x -ostrawberry-perl strawberry-perl.zip | Write-Debug
		    Remove-Item .\strawberry-perl.zip

	    }

		
	    if((Test-Path .\cmake) -ne $true) {
			Write-Verbose "Get-ApacheBuildTools - downloading cmake..."
		    Invoke-WebRequest $cMakeSource -UseBasicParsing -OutFile .\cmake.zip
			Write-Verbose "Get-ApacheBuildTools - extracting cmake to cmake_temp..."
		    .\7za.exe x -ocmake_temp cmake.zip | Write-Debug
			Write-Verbose "Get-ApacheBuildTools - move required files from cmake_temp to cmake..."
		    (Get-Item .\cmake_temp).GetDirectories().FullName | Move-Item -Destination .\cmake
		    (Get-Item .\cmake_temp) | Remove-Item
		    Remove-Item .\cmake.zip

	    }

		
	    if((Test-Path .\awk.exe) -ne $true) {
			Write-Verbose "Get-ApacheBuildTools - downloading awk.."
            $awkSource = Get-RedirectedUrl $awkSource
            Invoke-WebRequest $awkSource -UseBasicParsing -OutFile .\awk.zip
			Write-Verbose "Get-ApacheBuildTools - extracting awk to awk_temp..." 
		    .\7za.exe x -oawk_temp awk.zip| Write-Debug
			Write-Verbose "Get-ApacheBuildTools - get awk.exe from awk_temp ..."
		    Copy-Item .\awk_temp\bin\awk.exe -Destination .\awk.exe
		    (Get-Item .\awk_temp) | Remove-Item -Recurse -Force
		    Remove-Item .\awk.zip

	    }

		
	    if((Test-Path .\nasm.exe) -ne $true) {
			Write-Verbose "Get-ApacheBuildTools - downloading nasm..."
		    Invoke-WebRequest $nasmSource -UseBasicParsing  -OutFile .\nasm.zip
			Write-Verbose "Get-ApacheBuildTools - extracting nasm to nasm_temp..."
		    .\7za.exe x -onasm_temp nasm.zip | Write-Debug
			Write-Verbose "Get-ApacheBuildTools - move required files from nasm_temp to nasm..."
		    (Get-Item .\nasm_temp).GetDirectories().GetFiles("nasm.exe").FullName | Copy-Item -Destination .\nasm.exe
		    (Get-Item .\nasm_temp) | Remove-Item -Recurse -Force
		    Remove-Item .\nasm.zip
	    }
		
	    if((Test-Path .\expat.exe) -ne $true) {
			Write-Verbose "Get-ApacheBuildTools - downloading expat..."
		    Invoke-WebRequest $expatSource -UseBasicParsing  -OutFile .\expat.exe
	    }
	}
	finally{
		Pop-Location
	}

	Write-Verbose "Get-ApacheBuildTools - no more tools required"
}

#
# Set_VisualStudioEnvironment.ps1
#
function Set-VisualStudioEnvironment() {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory=$True,ValueFromPipeline=$False,ValueFromPipelinebyPropertyName=$False)]
		[ValidateSet('12','14')]
		[string] $VSVersion,
		[Parameter(Mandatory=$True,ValueFromPipeline=$False,ValueFromPipelinebyPropertyName=$False)]
		[ValidateSet('x86','x64')]
		[string] $Platform
	)

	if([Environment].Is64BitProcess) {
		$registrySoftwarePath = "'HKLM:\SOFTWARE"
     }
	else{
		$registrySoftwarePath = "HKLM:\SOFTWARE\Wow6432Node"
	}

	$registryVSPath = Join-Path $registrySoftwarePath "Microsoft\VisualStudio\$VSVersion.0"
	
	$VSShellFolder = Get-ItemProperty -Path $registryVSPath -Name ShellFolder -ErrorAction SilentlyContinue | Select -ExpandProperty ShellFolder
	
	if($VSShellFolder -eq $null)
	{
		Write-Error "Set-VisualStudioEnvironment - Visual Studio $VSVersion is not installed or detection failed"
		exit
	}

	Push-Location (Join-Path $VSShellFolder "VC")
	Invoke-Environment "call vcvarsall.bat $Platform" -Force
	Pop-Location


}


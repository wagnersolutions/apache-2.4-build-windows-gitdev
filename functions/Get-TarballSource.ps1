#
# Get_TarballSource.ps1
#
function Get-TarballSource {
  [CmdletBinding()]
  param
  (
    [string]$Source,
    [string]$Target
  )

  process {
    
    if($Source -eq $null){
        Write-Error "source missing"
        return
    }

    if($Target -eq $null){
        Write-Error "target missing"
        return
    }

    $tempFolder = New-Item -Path (Join-Path $env:TEMP ([System.Guid]::NewGuid().toString())) -ItemType Directory 
	Write-Debug "Get-TarballSource - temp folder:  $tempFolder"

    try{	
        $Source = Get-RedirectedUrl $Source
        Write-Verbose "Get-TarballSource - get $Source"


		Push-Location $tempFolder
		try{

            Invoke-WebRequest  $Source -UseBasicParsing -OutFile "source.tar.gz"
			& 7za.exe x source.tar.gz | Write-Debug
			$tarPath = Split-Path ($tempFolder.GetFiles("*.tar") | Select -First 1) -Leaf
			Write-Debug "Get-TarballSource - tar folder:  $tarPath"
			& 7za.exe x -oextract "$tarPath" | Write-Debug

            $ExtractedFolders = (Get-Item .\extract).GetDirectories() | Select -ExpandProperty FullName

        } finally {
			Pop-Location
		}

        $extractedFolders | % { Move-Item $_ $Target }
    }
    finally{
        Remove-Item $tempFolder -Recurse -ErrorAction SilentlyContinue
    }
  }

}

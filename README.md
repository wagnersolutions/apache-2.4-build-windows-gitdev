# Apache 2.4 Build for Windows#

This project provides a powershell based tool to build Apache 2.4 on Windows.

If you ever tried to build apache on Windows, you may also find out that this is not so easy. For example you have to download various tools and sources. 
With the powershell scripts of this project you can automate the necessary tasks. The scripts performs the following tasks for you:

* download of required build tools
* download of latest Apache and library sources
* applying necessary patches
* cmake based build of Apache
* tests the integrity of build result (required files, file versions)
* optionally packaging into zip file

To use the scripts you only need a Windows x64 System and Visual Studio (VS2013 or VS2015) with VC++ installed.

### Just looking for a Apache Windows download?###

If you are just looking for a Apache download for Windows you should consider a well-known site like this: [http://www.apachelounge.com/](http://www.apachelounge.com/)

Otherwise you can choose to trust us and use the builds that we publish in the download section. We are trying to update it as fast as possible in future (especially for openssl).

## Quickstart ##

1. Download the repository content
2. Extract the repository file to a folder
3. Start the script Invoke-ApacheBuild.ps1


In the download section of this repository you can find a build version of apache created with this build tool.

## Configuration ##

The Script Invoke-ApacheBuild.ps1 has several options you can use to customize for your requirements:

* -VSVersion:
Defaults to Visual Studio 2013 (Version 12), you can change it to 14 for Visual Studio 2015. Installation path will be autodetected. 

* -Platform:
Defaults to x64, you can change it to x86 for 32Bit build.

* -ToolsPath:
The required build tools will be downloaded to this path. Default is .\bin in current work directory.

* -SourcePath: 
Sources will be searched and downloaded to this directory. Default is .\source in current work directory.

* -BuildPath: 
Build files will be stored in this folder. Default is .\build in current work directory.

* -InstallPrefix: 
The default output path is c:\Apache24, that you can customize it with this option. 

* -KeepBuild: 
By default the script will delete the last build. If you set this option this you can perform a rebuild based on your latest changes.

* -NoSourceDownload: 
If you set the -NoSourceDownload option you avoid that sources are updated from source servers.

* -CreatePackage:
With this option the script will package the build apache into a zip file and tags it with the apache and openssl version (plus VS Version and platform).

## Include Patches ##

To apply custom patches you can place them in folder "patches". 
Then add a line to the file Apply-Patches.ps1 like this


```
#!powershell

Apply-Patch "httpd\bug_45922_ap24.patch"

```

Instead of the patch files you could also directly update the files in the src Folder, of course.


## Limitations ##

* currently the libraries lua and libxml are not build, so modules for lua and XML will be missing.
* README files and documentation is currently not copied to the output directory.
* with Visual Studio 2015 ApacheMonitor will not be build to due an Apache issue with VC14.
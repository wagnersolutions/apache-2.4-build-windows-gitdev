﻿[CmdletBinding()]
Param(
  [string] $PatchPath = "patches"
)

######################################################################################
# Load of functions
######################################################################################

	# required local function to find directory for other scripts
	function Get-ScriptDirectory {   
	# .SYNOPSIS  
	#   Return the current script directory path, compatible with PrimalScript 2009 
	#   Equivalent to VBscript fso.GetParentFolderName(WScript.ScriptFullName) 
	#   Requires PowerShell 2.0 
	#     
	# .DESCRIPTION 
	#   Author   : Jean-Pierre.Paradis@fsa.ulaval.ca 
	#   Date     : March 31, 2010 
	#   Version  : 1.01 
	# # .LINK  
	#   http://blog.sapien.com/index.php/2009/09/02/powershell-hosting-and-myinvocation/       

		if (Test-Path variable:\hostinvocation){
			$FullPath=$hostinvocation.MyCommand.Path}     
		Else {         
			$FullPath=(get-variable myinvocation -scope script).value.Mycommand.Definition 
		}         
    
		if (Test-Path $FullPath) {         
			return (Split-Path $FullPath)          
		}     
		Else{         
			$FullPath=(Get-Location).path         
			Write-Warning ("Get-ScriptDirectory: Powershell Host <" + $Host.name + "> may not be compatible with this function, the current directory <" + $FullPath + "> will be used.")         
			return $FullPath        
		} 
	}

Push-Location (Join-Path (Get-ScriptDirectory) functions) -ErrorAction Stop
. .\Load-ApacheBuildFunctions.ps1
Pop-Location 

######################################################################################
# Apply patches
######################################################################################


Push-Location $PatchPath

#Apply-Patch "httpd\ApacheMonitor.patch"

Pop-Location

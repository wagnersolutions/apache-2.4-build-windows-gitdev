﻿
[CmdletBinding()]
Param(
  [string]$InstallPrefix = "c:\Apache24",
  [string] $ToolsPath = ".\bin",
  [string] $BuildPath = ".\build",
  [string] $SourcePath = ".\source",
  [string] $PatchPath = "",

  [ValidateSet('12','14')]
  [string] $VSVersion = "12",

  [ValidateSet('x86','x64')]
  [string] $Platform = "x64",

  [switch]$NoSourceDownload,
  [switch]$KeepBuild,
  [switch]$CreatePackage
  )

$ErrorActionPreference = "Stop"

######################################################################################
# Load of functions
######################################################################################

	# required local function to find directory for other scripts
	function Get-ScriptDirectory {   
	# .SYNOPSIS  
	#   Return the current script directory path, compatible with PrimalScript 2009 
	#   Equivalent to VBscript fso.GetParentFolderName(WScript.ScriptFullName) 
	#   Requires PowerShell 2.0 
	#     
	# .DESCRIPTION 
	#   Author   : Jean-Pierre.Paradis@fsa.ulaval.ca 
	#   Date     : March 31, 2010 
	#   Version  : 1.01 
	# # .LINK  
	#   http://blog.sapien.com/index.php/2009/09/02/powershell-hosting-and-myinvocation/       

		if (Test-Path variable:\hostinvocation){
			$FullPath=$hostinvocation.MyCommand.Path}     
		Else {         
			$FullPath=(get-variable myinvocation -scope script).value.Mycommand.Definition 
		}         
    
		if (Test-Path $FullPath) {         
			return (Split-Path $FullPath)          
		}     
		Else{         
			$FullPath=(Get-Location).path         
			Write-Warning ("Get-ScriptDirectory: Powershell Host <" + $Host.name + "> may not be compatible with this function, the current directory <" + $FullPath + "> will be used.")         
			return $FullPath        
		} 
	}

Push-Location (Join-Path (Get-ScriptDirectory) functions) -ErrorAction Stop
. .\Load-ApacheBuildFunctions.ps1
Pop-Location 

######################################################################################
# Path handling
######################################################################################

# location of this script
$scriptDirectory = Get-ScriptDirectory
Write-Verbose "Invoke-ApacheBuild: Location of start script: '$scriptDirectory'"

$BuildPath = Join-Path $BuildPath ("VS$VSVersion" + "_" + "$Platform")

Write-Verbose "Invoke-ApacheBuild: tools path: $toolspath"
if((Test-Path $toolspath) -ne $true ) {
	New-Item $toolspath -ItemType Directory | Out-Null
}

Write-Verbose "Invoke-ApacheBuild: build path: $buildpath"
Write-Verbose "Invoke-ApacheBuild: source path: $sourcepath"

Write-Verbose "Invoke-ApacheBuild: resolving patch directory"

if($PatchPath -eq "") {
	$Global:PatchPath = Join-Path $scriptDirectory .\patch
} else{
	$Global:PatchPath = $PatchPath
	Resolve-Path $Global:PatchPath | Out-Null
}


######################################################################################
# prepare build
######################################################################################


# configure build environment
$ToolsPathResolved = (Resolve-Path $ToolsPath).Path
Push-Location $scriptDirectory
./Set-ApacheBuildEnvironment -VSVersion $VSVersion -Platform $Platform -ToolsPath $ToolsPathResolved -Autodownload
Pop-Location

# download required sources
if($NoSourceDownload.IsPresent -ne $true) {
	Write-Verbose "Invoke-ApacheBuild: starting download of required sources..."
	Get-ApacheSources $SourcePath

	
    if($VSVersion -eq "14") {
	    Write-Warning "Get-ApacheSources - ApacheMonitor will not be enabled for VC14"
    } else {
	    Write-Verbose "Get-ApacheSources - Patching httpd - ApacheMonitor"
	    push-Location (Join-Path $SourcePath httpd)
	    Apply-Patch "httpd\ApacheMonitor.patch"
	    Pop-Location
    }
}

######################################################################################
# cleanup previous builds
######################################################################################

if((Test-Path $InstallPrefix) -eq $true){
	Write-Verbose "Invoke-ApacheBuild: removing existing apache at $InstallPrefix"
	Remove-Item $InstallPrefix -Recurse -Force
}

if($KeepBuild.IsPresent -ne $true) {
	if((Test-Path $BuildPath) -eq $true){
		Write-Verbose "Invoke-ApacheBuild: removing previous build at $BuildPath"
		Remove-Item $BuildPath -Recurse -Force
	}
}

######################################################################################
# build
######################################################################################

#zlib
Write-Verbose "Invoke-ApacheBuild: building zlib..."
Invoke-Make -SourcePath (Join-Path $SourcePath "zlib") -BuildPath (Join-Path $BuildPath "zlib") -InstallPrefix $InstallPrefix 

#pcre
Write-Verbose "Invoke-ApacheBuild: building pcre..."
Invoke-Make -SourcePath (Join-Path $SourcePath "pcre") -BuildPath (Join-Path $BuildPath "pcre") -InstallPrefix $InstallPrefix -Arguments ("-DBUILD_SHARED_LIBS=ON" ,"-DPCRE_BUILD_TESTS=OFF", "-DPCRE_BUILD_PCRECPP=OFF" , "-DPCRE_BUILD_PCREGREP=OFF", "-DPCRE_SUPPORT_PCREGREP_JIT=OFF", "-DPCRE_SUPPORT_UTF=ON", "-DPCRE_SUPPORT_UNICODE_PROPERTIES=ON", "-DPCRE_NEWLINE=CRLF", "-DINSTALL_MSVC_PDB=OFF")

#openssl
Write-Verbose "Invoke-ApacheBuild: building openssl..."
Invoke-OpensslBuild -SourcePath (Join-Path $SourcePath "openssl") -BuildPath (Join-Path $BuildPath "openssl") -InstallPrefix $InstallPrefix -Platform $Platform

#apr
Write-Verbose "Invoke-ApacheBuild: building apr."
Invoke-Make -SourcePath (Join-Path $SourcePath "apr") -BuildPath (Join-Path $BuildPath "apr") -InstallPrefix $InstallPrefix -Arguments ("-DMIN_WINDOWS_VER=0x0600", "-DAPR_HAVE_IPV6=ON", "-DAPR_INSTALL_PRIVATE_H=ON", "-DAPR_BUILD_TESTAPR=OFF", "-DINSTALL_PDB=OFF")

#apr-util
Write-Verbose "Invoke-ApacheBuild: building apr-util..."
$openSSLRootOption = "-DOPENSSL_ROOT_DIR="+ $InstallPrefix
Invoke-Make -SourcePath (Join-Path $SourcePath "apr-util") -BuildPath (Join-Path $BuildPath "apr-util") -InstallPrefix $InstallPrefix -Arguments ($openSSLRootOption, "-DAPU_HAVE_CRYPTO=ON", "-DAPR_BUILD_TESTAPR=OFF" ,"-DINSTALL_PDB=OFF")

#httpd
Write-Verbose "Invoke-ApacheBuild: building httpd..."
Invoke-Make -SourcePath (Join-Path $SourcePath "httpd") -BuildPath (Join-Path $BuildPath "httpd") -InstallPrefix $InstallPrefix -Arguments ("-DENABLE_MODULES=i", ,"-DINSTALL_PDB=OFF")


######################################################################################
# test the result
######################################################################################
Write-Verbose "Invoke-ApacheBuild: testing file versions..."
Test-ApacheBuildFileVersions $InstallPrefix

######################################################################################
# tag the result
######################################################################################

Write-Verbose "Invoke-ApacheBuild: tagging build..."

$longVersionName = Get-ApacheVersionTag -SourcePath $SourcePath -VSVersion $VSVersion -Platform $Platform -Long
$shortVersionName = Get-ApacheVersionTag -SourcePath $SourcePath -VSVersion $VSVersion -Platform $Platform 

Set-Content $InstallPrefix\build.info -Value $longVersionName

if($CreatePackage.IsPresent) {

	Write-Verbose "Invoke-ApacheBuild: creating build archive..."
	$archiveFile = Join-Path $BuildPath "$shortVersionName.zip"
	7za.exe a $archiveFile "$InstallPrefix" 

	Get-Item $archiveFile

} else {
	Get-Item $InstallPrefix
}